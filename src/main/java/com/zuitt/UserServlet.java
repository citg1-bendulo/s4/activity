package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletContext;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3507288256115388268L;

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		String firstName= req.getParameter("firstName");
		String lastName= req.getParameter("lastName");
		String email= req.getParameter("email");
		String contact= req.getParameter("contact");
		
		//set data for system properties
		System.setProperty("firstName", firstName);
		
		//set data for httpsession
		HttpSession session=req.getSession();
		session.setAttribute("lastName", lastName );
		
		//set data for context params
		ServletContext srvContext=getServletContext();
		srvContext.setAttribute("Email",email );
		
		//passing the data via redirecting http 
		res.sendRedirect("details?contact="+contact);
	}
}

