package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletContext;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class DetailsServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4580377562697012475L;

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		ServletContext srvContext = getServletContext();
		PrintWriter out=res.getWriter();
		//get the value through system property
		String firstName=System.getProperty("firstName");
		
		//get the value through httpsession & remember to add to string method too
		HttpSession session=req.getSession();		
		String lastName= session.getAttribute("lastName").toString();
		
		//get the value through Context params		
		String email=(String) srvContext.getAttribute("Email");
		
		//get the value through initial params
		String branding =(String) srvContext.getInitParameter("Branding");
		
		//get the value through redirect(http request params)
		String contact= req.getParameter("contact"); 
		
		out.println("<h1>Welcome To Phone Book</h1>"
		+"<p>First Name:"+firstName+"</p>"
		+"<p>Last Name:"+lastName+"</p>"
		+"<p>Email:"+email+"</p>"
		+"<p>Contact:"+contact+"</p>"
		+"<p>Branding:"+branding+"</p>");
		
	}
}
